﻿using Flux.Extensions;
using Flux.WinForm.Actions;
using Flux.WinForm.Commons;
using Flux.WinForm.Models;
using Flux.WinForm.Stores;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Flux.WinForm
{
    public partial class MainForm : Form, IFluxViewFor<TodoStore>
    {
        private volatile bool _isUpdating;

        public MainForm()
        {
            InitializeComponent();

            Fluxs.Dispatcher = new AsyncDispatcher();
            Fluxs.StoreResolver.Register(new TodoStore());

            this.OnChange(OnChangeStore);

            Load += (x, y) => this.Dispatch(new GetAllTodosAction());
            lstTodos.ItemCheck += ItemCheckCommand;

            txtNewTodo.KeyDown += (x, y) => NewTodoKeyDown(y);
            btnAdd.Click += (x, y) => DispatchNewToDo();

            btnFilterAll.Click += (x, y) => this.Dispatch(new GetAllTodosAction());
            btnFilterActive.Click += (x, y) => this.Dispatch(new GetActiveTodosAction());
            btnFilterCompleted.Click += (x, y) => this.Dispatch(new GetCompletedTodosAction());
            btnClearCompleted.Click += (x, y) => this.Dispatch(new ClearCompletedTodosAction());
        }

        private void DispatchNewToDo()
        {
            this.Dispatch(new CreateTodoAction { Description = txtNewTodo.Text });

            txtNewTodo.Text = string.Empty;
        }

        private void ItemCheckCommand(object x, ItemCheckEventArgs y)
        {
            // Required so we don't end up dispatching again
            // when changing the check state of an inserted item
            if (_isUpdating)
            {
                return;
            }

            var id = (long)((ListView)x).Items[y.Index].Tag;
            this.Dispatch(new UpdateTodoAction
            {
                TodoId = id,
                Completed = y.NewValue == CheckState.Checked
            });
        }

        private void NewTodoKeyDown(KeyEventArgs y)
        {
            if (y.KeyCode == Keys.Return)
            {
                DispatchNewToDo();
            }
        }

        private void OnChangeStore(TodoStore store)
        {
            _isUpdating = true;

            lstTodos.Items.Clear();
            var toDos = store.Todos
                .OrderBy(x => x.CreatedAt)
                .Select(x => x.ToListViewItem())
                .ToArray();
            lstTodos.Items.AddRange(toDos);

            lblItemsLeft.Text = store.AllTodos.ItemsRemaining();

            btnFilterAll.Enabled = !store.Is(TodoFilters.All);
            btnFilterActive.Enabled = !store.Is(TodoFilters.Active);
            btnFilterCompleted.Enabled = !store.Is(TodoFilters.Complete);

            _isUpdating = false;
        }
    }
}