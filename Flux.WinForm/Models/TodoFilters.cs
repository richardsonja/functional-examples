﻿namespace Flux.WinForm.Models
{
    public enum TodoFilters
    {
        All,
        Active,
        Complete
    }
}