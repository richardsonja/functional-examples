﻿using System;

namespace Flux.WinForm.Models
{
    public class Todo
    {
        public DateTime CreatedAt { get; set; }

        public string Description { get; set; }

        public long Id { get; set; }

        public bool IsComplete { get; set; }
    }
}