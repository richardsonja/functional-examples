﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flux.WinForm.Models;
using Flux.WinForm.Stores;

namespace Flux.WinForm.Commons
{
   public static  class TodoStoreExtensions
   {
       public static bool Is(this TodoStore store, TodoFilters filter) => store.ActiveFilter == filter;
   }
}
