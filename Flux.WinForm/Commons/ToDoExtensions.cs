﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Flux.WinForm.Models;

namespace Flux.WinForm.Commons
{
   public static class ToDoExtensions
    {
        public static string ItemsRemaining(this IEnumerable<Todo> todos, string suffix = "item(s) left") => $"{todos.Count()} {suffix?.Trim()}";

        public static ListViewItem ToListViewItem(this Todo x) =>
            new(new[]
            {
                x.Id.ToString(CultureInfo.InvariantCulture),
                x.Description
            })
            {
                Tag = x.Id,
                Checked = x.IsComplete
            };
    }
}
