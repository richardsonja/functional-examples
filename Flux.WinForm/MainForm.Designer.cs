﻿
namespace Flux.WinForm
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNewTodo = new System.Windows.Forms.TextBox();
            this.lblNewTodo = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lstTodos = new System.Windows.Forms.ListView();
            this.colId = new System.Windows.Forms.ColumnHeader();
            this.colDescription = new System.Windows.Forms.ColumnHeader();
            this.lblItemsLeft = new System.Windows.Forms.Label();
            this.btnClearCompleted = new System.Windows.Forms.Button();
            this.btnFilterCompleted = new System.Windows.Forms.Button();
            this.btnFilterActive = new System.Windows.Forms.Button();
            this.btnFilterAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNewTodo
            // 
            this.txtNewTodo.Location = new System.Drawing.Point(98, 10);
            this.txtNewTodo.Name = "txtNewTodo";
            this.txtNewTodo.Size = new System.Drawing.Size(559, 27);
            this.txtNewTodo.TabIndex = 0;
            // 
            // lblNewTodo
            // 
            this.lblNewTodo.AutoSize = true;
            this.lblNewTodo.Location = new System.Drawing.Point(13, 17);
            this.lblNewTodo.Name = "lblNewTodo";
            this.lblNewTodo.Size = new System.Drawing.Size(79, 20);
            this.lblNewTodo.TabIndex = 1;
            this.lblNewTodo.Text = "New ToDo";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(663, 8);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(94, 29);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // lstTodos
            // 
            this.lstTodos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTodos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstTodos.CheckBoxes = true;
            this.lstTodos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colId,
            this.colDescription});
            this.lstTodos.GridLines = true;
            this.lstTodos.HideSelection = false;
            this.lstTodos.Location = new System.Drawing.Point(13, 60);
            this.lstTodos.Name = "lstTodos";
            this.lstTodos.Size = new System.Drawing.Size(757, 369);
            this.lstTodos.TabIndex = 3;
            this.lstTodos.UseCompatibleStateImageBehavior = false;
            this.lstTodos.View = System.Windows.Forms.View.Details;
            // 
            // colId
            // 
            this.colId.Text = "Id";
            // 
            // colDescription
            // 
            this.colDescription.Text = "Discription";
            this.colDescription.Width = 400;
            // 
            // lblItemsLeft
            // 
            this.lblItemsLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblItemsLeft.AutoSize = true;
            this.lblItemsLeft.Location = new System.Drawing.Point(12, 447);
            this.lblItemsLeft.Name = "lblItemsLeft";
            this.lblItemsLeft.Size = new System.Drawing.Size(83, 20);
            this.lblItemsLeft.TabIndex = 4;
            this.lblItemsLeft.Text = "0 items left";
            // 
            // btnClearCompleted
            // 
            this.btnClearCompleted.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearCompleted.Location = new System.Drawing.Point(637, 447);
            this.btnClearCompleted.Name = "btnClearCompleted";
            this.btnClearCompleted.Size = new System.Drawing.Size(133, 29);
            this.btnClearCompleted.TabIndex = 5;
            this.btnClearCompleted.Text = "Clear Completed";
            this.btnClearCompleted.UseVisualStyleBackColor = true;
            // 
            // btnFilterCompleted
            // 
            this.btnFilterCompleted.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFilterCompleted.Location = new System.Drawing.Point(517, 447);
            this.btnFilterCompleted.Name = "btnFilterCompleted";
            this.btnFilterCompleted.Size = new System.Drawing.Size(94, 29);
            this.btnFilterCompleted.TabIndex = 6;
            this.btnFilterCompleted.Text = "Completed";
            this.btnFilterCompleted.UseVisualStyleBackColor = true;
            // 
            // btnFilterActive
            // 
            this.btnFilterActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFilterActive.Location = new System.Drawing.Point(417, 447);
            this.btnFilterActive.Name = "btnFilterActive";
            this.btnFilterActive.Size = new System.Drawing.Size(94, 29);
            this.btnFilterActive.TabIndex = 7;
            this.btnFilterActive.Text = "Active";
            this.btnFilterActive.UseVisualStyleBackColor = true;
            // 
            // btnFilterAll
            // 
            this.btnFilterAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFilterAll.Location = new System.Drawing.Point(317, 447);
            this.btnFilterAll.Name = "btnFilterAll";
            this.btnFilterAll.Size = new System.Drawing.Size(94, 29);
            this.btnFilterAll.TabIndex = 8;
            this.btnFilterAll.Text = "All";
            this.btnFilterAll.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 486);
            this.Controls.Add(this.btnFilterAll);
            this.Controls.Add(this.btnFilterActive);
            this.Controls.Add(this.btnFilterCompleted);
            this.Controls.Add(this.btnClearCompleted);
            this.Controls.Add(this.lblItemsLeft);
            this.Controls.Add(this.lstTodos);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblNewTodo);
            this.Controls.Add(this.txtNewTodo);
            this.Name = "MainForm";
            this.Text = "ToDo Example App";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNewTodo;
        private System.Windows.Forms.Label lblNewTodo;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListView lstTodos;
        private System.Windows.Forms.ColumnHeader colId;
        private System.Windows.Forms.ColumnHeader colDescription;
        private System.Windows.Forms.Label lblItemsLeft;
        private System.Windows.Forms.Button btnClearCompleted;
        private System.Windows.Forms.Button btnFilterCompleted;
        private System.Windows.Forms.Button btnFilterActive;
        private System.Windows.Forms.Button btnFilterAll;
    }
}

