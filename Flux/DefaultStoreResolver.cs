﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flux.Exceptions;

namespace Flux
{
  public  class DefaultStoreResolver: IStoreResolver
    {
        private readonly IDictionary<Type, Store> _stores = new Dictionary<Type, Store>();

        public void Register<T>(T store) where T : Store
        {
            var key = typeof(T);

            if (!_stores.ContainsKey(key))
            {
                _stores.Add(key, null);
            }

            _stores[key] = store;
        }

        public T GetStore<T>() where T : Store
        {
            var key = typeof(T);

            return !_stores.ContainsKey(key) 
                ? throw new NullStoreException() 
                : (T) _stores[key];
        }
    }
}
