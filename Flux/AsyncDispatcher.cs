﻿using Flux.Models;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Flux
{
    public class AsyncDispatcher : Dispatcher, IAsyncDispatcher
    {
        public AsyncDispatcher()
        {
            SynchronizationContext = AsyncOperationManager.SynchronizationContext;
        }

        public SynchronizationContext SynchronizationContext { get; set; }

        public new void Dispatch<T>(T payload)
        {
            var callbacks = GetCallbacks<T>()
                .ToList();

            switch (payload)
            {
                case ChangePayload:
                    callbacks.ForEach(callback => SynchronizationContext.Post(x => callback((T)x), payload));
                    break;

                default:
                    callbacks.ForEach(callback => Task.Factory.StartNew(() => callback(payload)));
                    break;
            }
        }
    }
}