﻿using System;

namespace Flux
{
    public interface IDispatcher
    {
        void Dispatch<T>(T payload);

        void Register<T>(Action<T> callback);
    }
}