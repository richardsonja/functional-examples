﻿using System.Threading;

namespace Flux
{
    public interface IAsyncDispatcher : IDispatcher
    {
        SynchronizationContext SynchronizationContext { get; set; }
    }
}