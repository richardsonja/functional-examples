﻿using Flux.Exceptions;
using System;
using Flux.Models;

namespace Flux.Extensions
{
    public static class FluxViewForExtensions
    {
        public static void Dispatch<TStore, TPayload>(this IFluxViewFor<TStore> view, TPayload payload) where TStore : Store
        {
            var dispatcher = Fluxs.Dispatcher;

            if (dispatcher == null)
            {
                throw new NullDispatcherException();
            }

            dispatcher.Dispatch(payload);
        }

        public static void EmitChange<T>(this IFluxViewFor<T> view) where T : Store
        {
            Dispatch(view, new ChangePayload());
        }

        public static void OnChange<T>(this IFluxViewFor<T> view, Action<T> callback) where T : Store
        {
            var dispatcher = Fluxs.Dispatcher;

            if (dispatcher == null)
            {
                throw new NullDispatcherException();
            }

            var store = new Lazy<T>(GetStore<T>);

            dispatcher.Register<ChangePayload>(payload => callback(store.Value));

            EmitChange(view);
        }

        public static T GetStore<T>() where T : Store =>
            Fluxs.StoreResolver == null
                ? throw new NullStoreResolverException()
                : Fluxs.StoreResolver.GetStore<T>();
    }
}