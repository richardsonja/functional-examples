﻿using System;

namespace Flux.Exceptions
{
    public class NullStoreException : Exception
    {
        public NullStoreException()
            : base("Store has not been registered")
        {
        }
    }
}