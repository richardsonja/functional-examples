﻿using System;

namespace Flux.Exceptions
{
    public class NullDispatcherException : Exception
    {
        public NullDispatcherException() : base("Dispatcher has not been registered")
        {
        }
    }
}