﻿namespace Flux
{
    public interface IStore
    {
        void EmitChange();
    }
}