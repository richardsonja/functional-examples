﻿using Flux.Models;

namespace Flux
{
    public abstract class Store : IStore
    {
        protected Store()
        {
            Dispatcher = Fluxs.Dispatcher;
        }

        public IDispatcher Dispatcher { get; private set; }

        public void EmitChange()
        {
            Dispatcher.Dispatch(new ChangePayload());
        }
    }
}