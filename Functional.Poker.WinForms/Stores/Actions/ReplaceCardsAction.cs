﻿using System.Collections.Generic;
using Functional.Poker.Models;
using Functional.Poker.WinForms.Models;

namespace Functional.Poker.WinForms.Stores.Actions
{
    public class ReplaceCardsAction
    {
        public IEnumerable<Card> Cards { get; set; }

        public Player Player { get; set; }
    }
}