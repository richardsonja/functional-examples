﻿using Functional.Poker.WinForms.Models;

namespace Functional.Poker.WinForms.Stores.Actions
{
    public class NewDeckAction
    {
        public DeckDeliveryType DeckDeliveryType { get; set; }
        public int NumberOfCardsToDeal { get; set; }
    }
}