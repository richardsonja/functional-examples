﻿using Flux;
using Functional.Poker.Common;
using Functional.Poker.Models;
using Functional.Poker.WinForms.Models;
using Functional.Poker.WinForms.Stores.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Functional.Poker.WinForms.Stores
{
    public class GameStore : Store
    {
        private readonly int _fastTimeToWaitInMilliseconds = 200;

        private readonly int _timeToWaitInMilliseconds = 500;

        public GameStore()
        {
            Deck = new Deck(new List<Card>());
            DiscardDeck = new Deck(new List<Card>());
            Player1 = new Hand();
            Player2 = new Hand();
            Player3 = new Hand();
            Player4 = new Hand();

            Dispatcher.Register<NewDeckAction>(OnNewDeck);
            Dispatcher.Register<ReplaceCardsAction>(ReplaceCard);
        }

        public Deck Deck { get; private set; }

        public Deck DiscardDeck { get; private set; }

        public Hand Player1 { get; private set; }

        public Hand Player2 { get; private set; }

        public Hand Player3 { get; private set; }

        public Hand Player4 { get; private set; }

        private Tuple<Deck, Hand> Deal(Hand hand, int numberOfCardsToDeal)
        {
            var playerResults = Deck.DrawHand(hand, numberOfCardsToDeal);
            if (playerResults.IsLeft)
            {
                SetAllToNull();
                return null;
            }

            var results = playerResults.Match(x => x, _ => null);
            return results;
        }

        private void DealAllCardsAtOnceForEachPlayer(int numberOfCardsToDeal)
        {
            DealPlayer1(numberOfCardsToDeal);
            EmitChange();
            Thread.Sleep(_timeToWaitInMilliseconds);

            DealPlayer2(numberOfCardsToDeal);
            EmitChange();
            Thread.Sleep(_timeToWaitInMilliseconds);

            DealPlayer3(numberOfCardsToDeal);
            EmitChange();
            Thread.Sleep(_timeToWaitInMilliseconds);

            DealPlayer4(numberOfCardsToDeal);
        }

        private void DealOneCardsAtTimeForEachPlayer(int numberOfCardsToDeal)
        {
            for (var i = 0; i < numberOfCardsToDeal; i++)
            {
                DealPlayer1(1);
                EmitChange();
                Thread.Sleep(_fastTimeToWaitInMilliseconds);

                DealPlayer2(1);
                EmitChange();
                Thread.Sleep(_fastTimeToWaitInMilliseconds);

                DealPlayer3(1);
                EmitChange();
                Thread.Sleep(_fastTimeToWaitInMilliseconds);

                DealPlayer4(1);
                EmitChange();
                Thread.Sleep(_fastTimeToWaitInMilliseconds);
            }
        }

        private void DealPlayer1(int numberOfCardsToDeal)
        {
            var playerResults = Deal(Player1, numberOfCardsToDeal);
            if (playerResults == null) return;

            Deck = playerResults.Item1;
            Player1 = playerResults.Item2;
        }

        private void DealPlayer2(int numberOfCardsToDeal)
        {
            var playerResults = Deal(Player2, numberOfCardsToDeal);
            if (playerResults == null) return;

            Deck = playerResults.Item1;
            Player2 = playerResults.Item2;
        }

        private void DealPlayer3(int numberOfCardsToDeal)
        {
            var playerResults = Deal(Player3, numberOfCardsToDeal);
            if (playerResults == null) return;

            Deck = playerResults.Item1;
            Player3 = playerResults.Item2;
        }

        private void DealPlayer4(int numberOfCardsToDeal)
        {
            var playerResults = Deal(Player4, numberOfCardsToDeal);
            if (playerResults == null) return;

            Deck = playerResults.Item1;
            Player4 = playerResults.Item2;
        }

        private void OnNewDeck(NewDeckAction action)
        {
            Deck = DeckExtensions.StartDeck();
            DiscardDeck = new Deck(new List<Card>());
            Player1 = new Hand();
            Player2 = new Hand();
            Player3 = new Hand();
            Player4 = new Hand();

            switch (action.DeckDeliveryType)
            {
                case DeckDeliveryType.AllCardsToEachPlayer:
                    DealAllCardsAtOnceForEachPlayer(action.NumberOfCardsToDeal);
                    break;

                case DeckDeliveryType.OneCardAtTimeToEachPlayer:
                    DealOneCardsAtTimeForEachPlayer(action.NumberOfCardsToDeal);
                    break;

                default:
                    SetAllToNull();
                    break;
            }

            EmitChange();
        }

        private void ReplaceCard(ReplaceCardsAction action)
        {
            if (Deck == null || !action.Cards.Any())
            {
                return;
            }

            var countOfNewCards = action.Cards.Count();
            switch (action.Player)
            {
                case Player.PlayerOne:
                    Player1 = Player1.RemoveCard(action.Cards);
                    DealPlayer1(countOfNewCards);
                    break;

                case Player.PlayerTwo:
                    Player2 = Player2.RemoveCard(action.Cards);
                    DealPlayer2(countOfNewCards);
                    break;

                case Player.PlayerThree:
                    Player3 = Player3.RemoveCard(action.Cards);
                    DealPlayer3(countOfNewCards);
                    break;

                case Player.PlayerFour:
                    Player4 = Player4.RemoveCard(action.Cards);
                    DealPlayer4(countOfNewCards);
                    break;

                default:
                    SetAllToNull();
                    break;
            }

            if (Deck != null)
            {
                foreach (var card in action.Cards)
                {
                    DiscardDeck = DiscardDeck.Append(card);
                }
            }


            EmitChange();
        }

        private void SetAllToNull()
        {
            Deck = null;
            DiscardDeck = null;
            Player1 = null;
            Player2 = null;
            Player3 = null;
            Player4 = null;
        }
    }
}