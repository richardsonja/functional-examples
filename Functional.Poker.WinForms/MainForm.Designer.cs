﻿
namespace Functional.Poker.WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDeck = new System.Windows.Forms.GroupBox();
            this.btnNewDeck5But1AtTime = new System.Windows.Forms.Button();
            this.btnNewDeck5AtTime = new System.Windows.Forms.Button();
            this.lblDeckCount = new System.Windows.Forms.Label();
            this.lblDiscardCount = new System.Windows.Forms.Label();
            this.gbPlayer1 = new System.Windows.Forms.GroupBox();
            this.lblPlayer1High = new System.Windows.Forms.Label();
            this.listPlayer1 = new System.Windows.Forms.ListView();
            this.colPlayer1Card = new System.Windows.Forms.ColumnHeader();
            this.gbPlayer2 = new System.Windows.Forms.GroupBox();
            this.lblPlayer2High = new System.Windows.Forms.Label();
            this.listPlayer2 = new System.Windows.Forms.ListView();
            this.colPlayer2Card = new System.Windows.Forms.ColumnHeader();
            this.gbPlayer3 = new System.Windows.Forms.GroupBox();
            this.lblPlayer3High = new System.Windows.Forms.Label();
            this.listPlayer3 = new System.Windows.Forms.ListView();
            this.colPlayer3Card = new System.Windows.Forms.ColumnHeader();
            this.gbPlayer4 = new System.Windows.Forms.GroupBox();
            this.lblPlayer4High = new System.Windows.Forms.Label();
            this.listPlayer4 = new System.Windows.Forms.ListView();
            this.colPlayer4Card = new System.Windows.Forms.ColumnHeader();
            this.btnPlayer1Replace = new System.Windows.Forms.Button();
            this.gbDeck.SuspendLayout();
            this.gbPlayer1.SuspendLayout();
            this.gbPlayer2.SuspendLayout();
            this.gbPlayer3.SuspendLayout();
            this.gbPlayer4.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDeck
            // 
            this.gbDeck.Controls.Add(this.btnNewDeck5But1AtTime);
            this.gbDeck.Controls.Add(this.btnNewDeck5AtTime);
            this.gbDeck.Controls.Add(this.lblDeckCount);
            this.gbDeck.Location = new System.Drawing.Point(12, 12);
            this.gbDeck.Name = "gbDeck";
            this.gbDeck.Size = new System.Drawing.Size(1060, 93);
            this.gbDeck.TabIndex = 0;
            this.gbDeck.TabStop = false;
            this.gbDeck.Text = "Deck Info";
            // 
            // btnNewDeck5But1AtTime
            // 
            this.btnNewDeck5But1AtTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewDeck5But1AtTime.Location = new System.Drawing.Point(787, 58);
            this.btnNewDeck5But1AtTime.Name = "btnNewDeck5But1AtTime";
            this.btnNewDeck5But1AtTime.Size = new System.Drawing.Size(267, 29);
            this.btnNewDeck5But1AtTime.TabIndex = 3;
            this.btnNewDeck5But1AtTime.Text = "New Deck - 5 at time, 1 at time each";
            this.btnNewDeck5But1AtTime.UseVisualStyleBackColor = true;
            // 
            // btnNewDeck5AtTime
            // 
            this.btnNewDeck5AtTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewDeck5AtTime.Location = new System.Drawing.Point(787, 23);
            this.btnNewDeck5AtTime.Name = "btnNewDeck5AtTime";
            this.btnNewDeck5AtTime.Size = new System.Drawing.Size(267, 29);
            this.btnNewDeck5AtTime.TabIndex = 2;
            this.btnNewDeck5AtTime.Text = "New Deck - 5 at time, 5 at time";
            this.btnNewDeck5AtTime.UseVisualStyleBackColor = true;
            // 
            // lblDeckCount
            // 
            this.lblDeckCount.AutoSize = true;
            this.lblDeckCount.Location = new System.Drawing.Point(23, 32);
            this.lblDeckCount.Name = "lblDeckCount";
            this.lblDeckCount.Size = new System.Drawing.Size(119, 20);
            this.lblDeckCount.TabIndex = 0;
            this.lblDeckCount.Text = "Deck has 0 cards";
            // 
            // lblDiscardCount
            // 
            this.lblDiscardCount.AutoSize = true;
            this.lblDiscardCount.Location = new System.Drawing.Point(35, 68);
            this.lblDiscardCount.Name = "lblDiscardCount";
            this.lblDiscardCount.Size = new System.Drawing.Size(165, 20);
            this.lblDiscardCount.TabIndex = 1;
            this.lblDiscardCount.Text = "Discard pile has 0 cards";
            // 
            // gbPlayer1
            // 
            this.gbPlayer1.Controls.Add(this.btnPlayer1Replace);
            this.gbPlayer1.Controls.Add(this.lblPlayer1High);
            this.gbPlayer1.Controls.Add(this.listPlayer1);
            this.gbPlayer1.Location = new System.Drawing.Point(12, 133);
            this.gbPlayer1.Name = "gbPlayer1";
            this.gbPlayer1.Size = new System.Drawing.Size(250, 482);
            this.gbPlayer1.TabIndex = 2;
            this.gbPlayer1.TabStop = false;
            this.gbPlayer1.Text = "Player 1";
            // 
            // lblPlayer1High
            // 
            this.lblPlayer1High.AutoSize = true;
            this.lblPlayer1High.Location = new System.Drawing.Point(7, 27);
            this.lblPlayer1High.Name = "lblPlayer1High";
            this.lblPlayer1High.Size = new System.Drawing.Size(123, 20);
            this.lblPlayer1High.TabIndex = 5;
            this.lblPlayer1High.Text = "-------------------";
            // 
            // listPlayer1
            // 
            this.listPlayer1.CheckBoxes = true;
            this.listPlayer1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPlayer1Card});
            this.listPlayer1.HideSelection = false;
            this.listPlayer1.Location = new System.Drawing.Point(6, 60);
            this.listPlayer1.Name = "listPlayer1";
            this.listPlayer1.Size = new System.Drawing.Size(236, 356);
            this.listPlayer1.TabIndex = 4;
            this.listPlayer1.UseCompatibleStateImageBehavior = false;
            this.listPlayer1.View = System.Windows.Forms.View.Details;
            // 
            // colPlayer1Card
            // 
            this.colPlayer1Card.Text = "";
            this.colPlayer1Card.Width = 215;
            // 
            // gbPlayer2
            // 
            this.gbPlayer2.Controls.Add(this.lblPlayer2High);
            this.gbPlayer2.Controls.Add(this.listPlayer2);
            this.gbPlayer2.Location = new System.Drawing.Point(283, 133);
            this.gbPlayer2.Name = "gbPlayer2";
            this.gbPlayer2.Size = new System.Drawing.Size(250, 482);
            this.gbPlayer2.TabIndex = 3;
            this.gbPlayer2.TabStop = false;
            this.gbPlayer2.Text = "Player 2";
            // 
            // lblPlayer2High
            // 
            this.lblPlayer2High.AutoSize = true;
            this.lblPlayer2High.Location = new System.Drawing.Point(6, 27);
            this.lblPlayer2High.Name = "lblPlayer2High";
            this.lblPlayer2High.Size = new System.Drawing.Size(123, 20);
            this.lblPlayer2High.TabIndex = 6;
            this.lblPlayer2High.Text = "-------------------";
            // 
            // listPlayer2
            // 
            this.listPlayer2.CheckBoxes = true;
            this.listPlayer2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPlayer2Card});
            this.listPlayer2.HideSelection = false;
            this.listPlayer2.Location = new System.Drawing.Point(6, 60);
            this.listPlayer2.Name = "listPlayer2";
            this.listPlayer2.Size = new System.Drawing.Size(236, 356);
            this.listPlayer2.TabIndex = 5;
            this.listPlayer2.UseCompatibleStateImageBehavior = false;
            this.listPlayer2.View = System.Windows.Forms.View.Details;
            // 
            // colPlayer2Card
            // 
            this.colPlayer2Card.Text = "";
            this.colPlayer2Card.Width = 215;
            // 
            // gbPlayer3
            // 
            this.gbPlayer3.Controls.Add(this.lblPlayer3High);
            this.gbPlayer3.Controls.Add(this.listPlayer3);
            this.gbPlayer3.Location = new System.Drawing.Point(554, 133);
            this.gbPlayer3.Name = "gbPlayer3";
            this.gbPlayer3.Size = new System.Drawing.Size(250, 482);
            this.gbPlayer3.TabIndex = 4;
            this.gbPlayer3.TabStop = false;
            this.gbPlayer3.Text = "Player 3";
            // 
            // lblPlayer3High
            // 
            this.lblPlayer3High.AutoSize = true;
            this.lblPlayer3High.Location = new System.Drawing.Point(8, 27);
            this.lblPlayer3High.Name = "lblPlayer3High";
            this.lblPlayer3High.Size = new System.Drawing.Size(123, 20);
            this.lblPlayer3High.TabIndex = 7;
            this.lblPlayer3High.Text = "-------------------";
            // 
            // listPlayer3
            // 
            this.listPlayer3.CheckBoxes = true;
            this.listPlayer3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPlayer3Card});
            this.listPlayer3.HideSelection = false;
            this.listPlayer3.Location = new System.Drawing.Point(8, 61);
            this.listPlayer3.Name = "listPlayer3";
            this.listPlayer3.Size = new System.Drawing.Size(236, 356);
            this.listPlayer3.TabIndex = 0;
            this.listPlayer3.UseCompatibleStateImageBehavior = false;
            this.listPlayer3.View = System.Windows.Forms.View.Details;
            // 
            // colPlayer3Card
            // 
            this.colPlayer3Card.Text = "";
            this.colPlayer3Card.Width = 215;
            // 
            // gbPlayer4
            // 
            this.gbPlayer4.Controls.Add(this.lblPlayer4High);
            this.gbPlayer4.Controls.Add(this.listPlayer4);
            this.gbPlayer4.Location = new System.Drawing.Point(822, 133);
            this.gbPlayer4.Name = "gbPlayer4";
            this.gbPlayer4.Size = new System.Drawing.Size(250, 482);
            this.gbPlayer4.TabIndex = 5;
            this.gbPlayer4.TabStop = false;
            this.gbPlayer4.Text = "Player 4";
            // 
            // lblPlayer4High
            // 
            this.lblPlayer4High.AutoSize = true;
            this.lblPlayer4High.Location = new System.Drawing.Point(7, 27);
            this.lblPlayer4High.Name = "lblPlayer4High";
            this.lblPlayer4High.Size = new System.Drawing.Size(123, 20);
            this.lblPlayer4High.TabIndex = 8;
            this.lblPlayer4High.Text = "-------------------";
            // 
            // listPlayer4
            // 
            this.listPlayer4.CheckBoxes = true;
            this.listPlayer4.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colPlayer4Card});
            this.listPlayer4.HideSelection = false;
            this.listPlayer4.Location = new System.Drawing.Point(7, 60);
            this.listPlayer4.Name = "listPlayer4";
            this.listPlayer4.Size = new System.Drawing.Size(236, 356);
            this.listPlayer4.TabIndex = 0;
            this.listPlayer4.UseCompatibleStateImageBehavior = false;
            this.listPlayer4.View = System.Windows.Forms.View.Details;
            // 
            // colPlayer4Card
            // 
            this.colPlayer4Card.Text = "";
            this.colPlayer4Card.Width = 215;
            // 
            // btnPlayer1Replace
            // 
            this.btnPlayer1Replace.Location = new System.Drawing.Point(132, 435);
            this.btnPlayer1Replace.Name = "btnPlayer1Replace";
            this.btnPlayer1Replace.Size = new System.Drawing.Size(94, 29);
            this.btnPlayer1Replace.TabIndex = 6;
            this.btnPlayer1Replace.Text = "Replace";
            this.btnPlayer1Replace.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 698);
            this.Controls.Add(this.gbPlayer4);
            this.Controls.Add(this.gbPlayer3);
            this.Controls.Add(this.gbPlayer2);
            this.Controls.Add(this.gbPlayer1);
            this.Controls.Add(this.lblDiscardCount);
            this.Controls.Add(this.gbDeck);
            this.Name = "MainForm";
            this.Text = "Poker";
            this.gbDeck.ResumeLayout(false);
            this.gbDeck.PerformLayout();
            this.gbPlayer1.ResumeLayout(false);
            this.gbPlayer1.PerformLayout();
            this.gbPlayer2.ResumeLayout(false);
            this.gbPlayer2.PerformLayout();
            this.gbPlayer3.ResumeLayout(false);
            this.gbPlayer3.PerformLayout();
            this.gbPlayer4.ResumeLayout(false);
            this.gbPlayer4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDeck;
        private System.Windows.Forms.Button btnNewDeck5AtTime;
        private System.Windows.Forms.Label lblDeckCount;
        private System.Windows.Forms.Label lblDiscardCount;
        private System.Windows.Forms.Button btnNewDeck5But1AtTime;
        private System.Windows.Forms.GroupBox gbPlayer1;
        private System.Windows.Forms.GroupBox gbPlayer2;
        private System.Windows.Forms.GroupBox gbPlayer3;
        private System.Windows.Forms.GroupBox gbPlayer4;
        private System.Windows.Forms.ListView listPlayer1;
        private System.Windows.Forms.ColumnHeader colPlayer1Card;
        private System.Windows.Forms.ListView listPlayer2;
        private System.Windows.Forms.ColumnHeader colPlayer2Card;
        private System.Windows.Forms.ListView listPlayer3;
        private System.Windows.Forms.ColumnHeader colPlayer3Card;
        private System.Windows.Forms.ListView listPlayer4;
        private System.Windows.Forms.ColumnHeader colPlayer4Card;
        private System.Windows.Forms.Label lblPlayer1High;
        private System.Windows.Forms.Label lblPlayer2High;
        private System.Windows.Forms.Label lblPlayer3High;
        private System.Windows.Forms.Label lblPlayer4High;
        private System.Windows.Forms.Button btnPlayer1Replace;
    }
}