﻿namespace Functional.Poker.WinForms.Models
{
    public enum Player
    {
        PlayerOne,
        PlayerTwo,
        PlayerThree,
        PlayerFour
    }
}