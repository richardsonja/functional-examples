﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functional.Poker.WinForms.Models
{
    public enum DeckDeliveryType
    {
        AllCardsToEachPlayer,
        OneCardAtTimeToEachPlayer
    }
}
