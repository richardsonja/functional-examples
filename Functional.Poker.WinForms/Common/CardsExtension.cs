﻿using Functional.Poker.Models;
using System.Windows.Forms;

namespace Functional.Poker.WinForms.Common
{
    public static class CardsExtension
    {
        public static ListViewItem ToListViewItem(this Card card) =>
            new(new[]
            {
                card.ToString(),
                card.ToString()
            })
            {
                Tag = card.ToString()
            };
    }
}