﻿using System;
using Flux;
using Flux.Extensions;
using Functional.Poker.Models;
using Functional.Poker.Services;
using Functional.Poker.WinForms.Common;
using Functional.Poker.WinForms.Models;
using Functional.Poker.WinForms.Stores;
using Functional.Poker.WinForms.Stores.Actions;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Functional.Poker.WinForms
{
    public partial class MainForm : Form, IFluxViewFor<GameStore>
    {
        private readonly int _numberOfCardsToDeal;
        private volatile bool _isUpdating;

        public MainForm()
        {
            _numberOfCardsToDeal = 5;
            InitializeComponent();
            Fluxs.Dispatcher = new AsyncDispatcher();
            Fluxs.StoreResolver.Register(new GameStore());

            this.OnChange(OnChangeStore);
            btnNewDeck5AtTime.Click += (x, y) => DispatchNewDeck(DeckDeliveryType.AllCardsToEachPlayer);
            btnNewDeck5But1AtTime.Click += (x, y) => DispatchNewDeck(DeckDeliveryType.OneCardAtTimeToEachPlayer);

            btnPlayer1Replace.Click += (x, y) => ReplaceCards(listPlayer1, Player.PlayerOne);
        }

        private void ReplaceCards(ListView list, Player player)
        {
            var cardStringsToReplace = list
                .Items
                .Cast<ListViewItem>()
                .Where(item => item.Checked)
                .Select(item =>
                    GetPlayerHand(player)
                        .Cards
                        .First(x => x.ToString().Equals(item.Text, StringComparison.CurrentCultureIgnoreCase)))
                .ToList();

            this.Dispatch(new ReplaceCardsAction { Cards = cardStringsToReplace, Player = player});
        }

        private Hand GetPlayerHand(Player player)
        {
            var store = new Lazy<GameStore>(FluxViewForExtensions.GetStore<GameStore>);
            return player switch
            {
                Player.PlayerOne => store.Value.Player1,
                Player.PlayerTwo => store.Value.Player2,
                Player.PlayerThree => store.Value.Player3,
                Player.PlayerFour => store.Value.Player4,
                _ => throw new ArgumentOutOfRangeException(nameof(player), player, null)
            };
        }

        private static void SetPlayerHand(IEnumerable<Card> cards, ListView view, Label label)
        {
            var enumerable = cards.ToList();
            var player = enumerable
                .OrderBy(x => x.Suit)
                .ThenBy(x => x.Value)
                .Select(x => x.ToListViewItem())
                .ToArray();

            view.Items.AddRange(player);

            label.Text = enumerable.Count > 1
                ? enumerable.GetHandRank().ToString()
                : string.Empty;
        }

        private void DispatchNewDeck(DeckDeliveryType deliveryType)
        {
            this.Dispatch(new NewDeckAction { DeckDeliveryType = deliveryType, NumberOfCardsToDeal = _numberOfCardsToDeal });
        }

        private void OnChangeStore(GameStore store)
        {
            _isUpdating = true;
            listPlayer1.Items.Clear();
            listPlayer2.Items.Clear();
            listPlayer3.Items.Clear();
            listPlayer4.Items.Clear();
            lblPlayer1High.Text = string.Empty;
            lblPlayer2High.Text = string.Empty;
            lblPlayer3High.Text = string.Empty;
            lblPlayer4High.Text = string.Empty;
            if (store.Deck == null)
            {
                lblDeckCount.Text = "ERROR";
                lblDiscardCount.Text = "ERROR";
                return;
            }

            lblDeckCount.Text = $"Deck has {store.Deck.AvailableCards.Count} cards";
            lblDiscardCount.Text = $"Discard pile has {store.DiscardDeck.AvailableCards.Count} cards";

            SetPlayerHand(store.Player1.Cards, listPlayer1, lblPlayer1High);
            SetPlayerHand(store.Player2.Cards, listPlayer2, lblPlayer2High);
            SetPlayerHand(store.Player3.Cards, listPlayer3, lblPlayer3High);
            SetPlayerHand(store.Player4.Cards, listPlayer4, lblPlayer4High);

            _isUpdating = false;
        }
    }
}