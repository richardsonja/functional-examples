﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Flux.Tests.Fixtures;
using Moq;
using Xunit;

namespace Flux.Tests
{
    public class AsyncDispatcherTests : IClassFixture<AsyncDispatcherFixture>
    {
        private readonly AsyncDispatcherFixture _dispatcher;

        public AsyncDispatcherTests(AsyncDispatcherFixture dispatcher)
        {
            _dispatcher = dispatcher;
        }
        [Fact]
        public void AsyncDispatcher_Callback_ShouldNotThrowException()
        {
            _dispatcher.Reset();
            var callback = new Action<string>(Console.WriteLine);
            Action action = () => _dispatcher.AsyncDispatcher.Register(callback);
            action.Should().NotThrow();
        }
        [Fact]
        public void AsyncDispatcher_Callback_ShouldAllowSeveralRegisterWithoutThrownException()
        {
            _dispatcher.Reset();
            var callback1 = new Action<string>(Console.WriteLine);
            var callback2 = new Action<string>(Console.WriteLine);
            Action action = () =>
            {
                _dispatcher.AsyncDispatcher.Register(callback1);
                _dispatcher.AsyncDispatcher.Register(callback2);
            };
            action.Should().NotThrow();
        }
        [Fact]
        public void AsyncDispatcher_Callback_ShouldInvokeBothStringCallBack()
        {
            _dispatcher.Reset();
            var callback1 = new Mock<Action<string>>();
            var callback2 = new Mock<Action<string>>();
            _dispatcher.AsyncDispatcher.Register(callback1.Object);
            _dispatcher.AsyncDispatcher.Register(callback2.Object);
            _dispatcher.AsyncDispatcher.Dispatch("Test");

            callback1.Verify(x => x("Test"), Times.Once);
            callback2.Verify(x => x("Test"), Times.Once);
        }


        [Fact]
        public void AsyncDispatcher_Callback_ShouldInvokeCorrectOneOnly()
        {
            _dispatcher.Reset();
            var callback1 = new Mock<Action<string>>();
            var callback2 = new Mock<Action<int>>();
            _dispatcher.AsyncDispatcher.Register(callback1.Object);
            _dispatcher.AsyncDispatcher.Register(callback2.Object);
            _dispatcher.AsyncDispatcher.Dispatch("Test");

            callback1.Verify(x => x("Test"), Times.Once);
            callback2.Verify(x => x(It.IsAny<int>()), Times.Never);
        }
    }
}
