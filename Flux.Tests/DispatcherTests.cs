﻿using FluentAssertions;
using Flux.Tests.Fixtures;
using System;
using Xunit;

namespace Flux.Tests
{
    public class DispatcherTests : IClassFixture<DispatcherFixture>
    {
        private readonly DispatcherFixture _dispatcher;

        public DispatcherTests(DispatcherFixture dispatcher)
        {
            _dispatcher = dispatcher;
        }

        [Fact]
        public void Dispatcher_Callback_ShouldNotThrowException()
        {
            var callback = new Action<string>(Console.WriteLine);
            Action action = () => _dispatcher.Dispatcher.Register(callback);
            action.Should().NotThrow();
        }
    }
}