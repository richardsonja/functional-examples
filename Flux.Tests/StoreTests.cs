using System;
using Flux.Models;
using Flux.Tests.Fixtures;
using Moq;
using Xunit;

namespace Flux.Tests
{
    public class StoreTests: IClassFixture<StoreFixture>
    {
        private readonly StoreFixture _store;

        public StoreTests(StoreFixture store)
        {
            _store = store;
        }

        [Fact]
        public void Store_EmitChange()
        {
            _store.Reset();
            _store.Dispatcher.Reset();
            _store.Store.EmitChange();
            _store.Dispatcher.Verify(x=> x.Dispatch(It.IsAny<ChangePayload>()), Times.Once);
        }
    }
}
