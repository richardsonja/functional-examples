﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Flux.Tests.Fixtures;
using Xunit;
using Flux.Extensions;
using Flux.Models;
using Moq;

namespace Flux.Tests.Extensions
{
    public class FluxViewForExtensionsTests: IClassFixture<StoreFixture>, IClassFixture<FluxViewFixture>
    {
        private readonly StoreFixture _store;
        private readonly FluxViewFixture _view;

        public FluxViewForExtensionsTests(StoreFixture store, FluxViewFixture view)
        {
            _store = store;
            _view = view;
        }

        [Fact]
        public void FluxViewForExtensions_Dispatch_ShouldCallOnChangeAndUpdateTestResult()
        {
            _store.Reset();
            _view.Reset();
            var payload = new TestAction { Result = "Changed" };

            _view.View.Dispatch(payload);

            _store.Dispatcher.Verify(x => x.Dispatch(payload), Times.Once);
        }

        [Fact]
        public void FluxViewForExtensions_EmitChange_ShouldDoNothing()
        {
            _store.Reset();
            _view.Reset();
            _store.Dispatcher.Reset();

            _view.View.EmitChange();
            _store.Dispatcher.Verify(x => x.Dispatch(It.IsAny<ChangePayload>()), Times.Never);
        }
    }
}
