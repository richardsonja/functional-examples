﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Flux.Exceptions;
using Flux.Tests.Fixtures;
using Xunit;

namespace Flux.Tests
{
   public class DefaultStoreResolverTests:IClassFixture<DefaultStoreResolverFixture>
    {
        private readonly DefaultStoreResolverFixture _storeResolver;

        public DefaultStoreResolverTests(DefaultStoreResolverFixture storeResolver)
        {
            _storeResolver = storeResolver;
        }

        [Fact]
        public void RegisterShouldNotThrowException()
        {
            _storeResolver.Reset();
            var store = new TestStore();
            Action action = () => _storeResolver.StoreResolver.Register(store);
            action.Should().NotThrow();
        }


        [Fact]
        public void GetStoreShouldReturnRegisteredStore()
        {
            _storeResolver.Reset();
            var store = new TestStore();

            _storeResolver.StoreResolver.Register(store);

            var result = _storeResolver.StoreResolver.GetStore<TestStore>();

            store.Should().Be(result);
        }

        [Fact]
        public void GetStoreShouldReturnLastRegisteredStore()
        {
            _storeResolver.Reset();
            var store1 = new TestStore();
            var store2 = new TestStore();

            _storeResolver.StoreResolver.Register(store1);
            _storeResolver.StoreResolver.Register(store2);

            var result = _storeResolver.StoreResolver.GetStore<TestStore>();

            store2.Should().Be(result);
        }

        [Fact]
        public void GetStoreWithUnregisteredStoreShouldThrowStoreNotRegisteredException()
        {
            _storeResolver.Reset();
            Action action = () => _storeResolver.StoreResolver.GetStore<TestStore>();
            action.Should().Throw<NullStoreException>();
        }
    }
}
