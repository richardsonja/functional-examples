﻿using Moq;
using System;

namespace Flux.Tests.Fixtures
{
    public class StoreFixture : IDisposable, IReset
    {
        public Mock<IDispatcher> Dispatcher;
        public Store Store;

        public StoreFixture() => Reset();

        public void Dispose()
        {
            Dispatcher = null;
            Store = null;
        }

        public void Reset()
        {
            Dispatcher = new();
            Fluxs.Dispatcher = Dispatcher.Object;
            Store = new TestStore();
        }
        
    }
}