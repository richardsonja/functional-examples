﻿using System;

namespace Flux.Tests.Fixtures
{
    public class FluxViewFixture : IDisposable, IReset
    {
        public IFluxViewFor<TestStore> View;

        public FluxViewFixture()
        {
            Reset();
        }

        public void Dispose()
        {
            View = null;
        }

        public void Reset()
        {
            View = new TestView();
        }
    }
}