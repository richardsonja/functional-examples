﻿using System;

namespace Flux.Tests.Fixtures
{
    public class AsyncDispatcherFixture : IDisposable, IReset
    {
        public IAsyncDispatcher AsyncDispatcher;

        public AsyncDispatcherFixture()
        {
            Reset();
        }

        public void Dispose()
        {
            AsyncDispatcher = null;
        }

        public void Reset()
        {
            AsyncDispatcher = new AsyncDispatcher();
        }
    }
}