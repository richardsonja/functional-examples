﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flux.Extensions;

namespace Flux.Tests.Fixtures
{
   public  class TestView: IFluxViewFor<TestStore>
    {
       public string TestResult { get; private set; }

       public TestView()
       {
           this.OnChange(UpdateTestResult);
       }

       private void UpdateTestResult(TestStore store)
       {
           TestResult = store.TestResult;
       }
   }
}