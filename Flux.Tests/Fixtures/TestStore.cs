﻿namespace Flux.Tests.Fixtures
{
    public class TestStore : Store
    {
        public TestStore()
        {
            Dispatcher.Register<TestAction>(OnTestAction);
        }

        public string TestResult { get; private set; }

        private void OnTestAction(TestAction action)
        {
            TestResult = action.Result;

            EmitChange();
        }
    }
}