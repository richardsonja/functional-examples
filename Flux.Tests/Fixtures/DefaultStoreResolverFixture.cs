﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flux.Tests.Fixtures
{
  public  class DefaultStoreResolverFixture: IDisposable, IReset
    {
        public  IStoreResolver StoreResolver;

        public DefaultStoreResolverFixture()
        {
            Reset();
        }
        public void Dispose()
        {
            StoreResolver = null;
        }

        public void Reset()
        {
            StoreResolver = new DefaultStoreResolver();
        }
    }
}
