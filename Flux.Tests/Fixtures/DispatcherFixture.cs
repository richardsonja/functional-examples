﻿using System;

namespace Flux.Tests.Fixtures
{
    public class DispatcherFixture : IDisposable, IReset
    {
        public Dispatcher Dispatcher;

        public DispatcherFixture()
        {
            Reset();
        }

        public void Dispose()
        {
            Dispatcher = null;
        }

        public void Reset()
        {
            Dispatcher = new Dispatcher();
        }
    }
}