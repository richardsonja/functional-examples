﻿using Functional.Poker.Common;
using Functional.Poker.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Functional.Poker.Services
{
    public static class PokerScorer
    {
        // A list of ranks gives added flexibility to how hand ranks can be scored.
        // Each ranker has an Eval delegate that returns a bool
        public static HandRank GetHandRank(this IEnumerable<Card> cards) => 
            Rankings()
                .OrderByDescending(card => card.rank)
                .First(rule => rule.eval(cards))
                .rank;

        public static Card HighCard(this IEnumerable<Card> cards) => 
            cards.Aggregate((highCard, nextCard) => 
                nextCard.Value > highCard.Value 
                    ? nextCard 
                    : highCard);

        private static int CountOfAKind(this IEnumerable<Card> cards, int num) => 
            cards
                .ToKindAndQuantities()
                .Count(c => c.Value == num);

        private static bool HasFlush(this IEnumerable<Card> cards) => 
            cards
                .All(c => cards.First().Suit == c.Suit);

        private static bool HasFourOfAKind(this IEnumerable<Card> cards) => HasOfAKind(cards, 4);

        private static bool HasFullHouse(this IEnumerable<Card> cards) => 
            HasThreeOfAKind(cards)
            && HasPair(cards);

        private static bool HasOfAKind(this IEnumerable<Card> cards, int num) => 
            cards
                .ToKindAndQuantities()
                .Any(c => c.Value == num);

        private static bool HasPair(this IEnumerable<Card> cards) => HasOfAKind(cards, 2);

        private static bool HasRoyalFlush(this IEnumerable<Card> cards) => 
            HasFlush(cards) 
            && cards.All(c => c.Value > CardValue.Nine);

        private static bool HasStraight(this IEnumerable<Card> cards) => 
            cards
                .OrderBy(card => card.Value)
                .SelectConsecutive((n, next) => n.Value + 1 == next.Value)
                .All(value => value);

        private static bool HasStraightFlush(this IEnumerable<Card> cards) =>
            HasStraight(cards) 
            && HasFlush(cards);

        private static bool HasThreeOfAKind(this IEnumerable<Card> cards) => HasOfAKind(cards, 3);

        private static bool HasTwoPair(this IEnumerable<Card> cards) => CountOfAKind(cards, 2) == 2;

        private static List<(Func<IEnumerable<Card>, bool> eval, HandRank rank)> Rankings() =>
           new List<(Func<IEnumerable<Card>, bool> eval, HandRank rank)>
           {
               (HasRoyalFlush, HandRank.RoyalFlush),
               (HasStraightFlush, HandRank.StraightFlush),
               (HasFourOfAKind, HandRank.FourOfAKind),
               (HasFullHouse, HandRank.FullHouse),
               (HasFlush, HandRank.Flush),
               (HasStraight, HandRank.Straight),
               (HasThreeOfAKind, HandRank.ThreeOfAKind),
               (HasTwoPair, HandRank.TwoPair),
               (HasPair, HandRank.Pair),
               (cards => true, HandRank.HighCard),
           };
    }
}