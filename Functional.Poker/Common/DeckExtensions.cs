﻿using Functional.Poker.Models;
using LanguageExt;
using System;
using System.Collections.Generic;
using System.Linq;
using static LanguageExt.Prelude;

namespace Functional.Poker.Common
{
    public static class DeckExtensions
    {
        public static Tuple<Deck, Hand> DrawCardForHand(this Deck deck, Hand hand)
        {
            var cards = deck.AvailableCards.Clone().ToList();
            var card = cards.First();
            cards.Remove(card);

            return new Tuple<Deck, Hand>(new Deck(cards), hand.AddCard(card));
        }

        public static Deck Append(this Deck deck, Card card)
        {
            var cards = deck.AvailableCards.Clone().Append(card).ToList();
            
            return new(cards);
        }

        public static Either<string, Tuple<Deck, Hand>> DrawHand(this Deck deck, Hand hand, int numberOfCards)
        {
            if (numberOfCards < 1) return Left<string, Tuple<Deck, Hand>>("Number of cards is too small");

            return numberOfCards > deck.AvailableCards.Count
                ? Left<string, Tuple<Deck, Hand>>("Requested Number of cards is more than what is available")
                : Right<string, Tuple<Deck, Hand>>(deck.DrawHand(hand, numberOfCards, 1));
        }

        public static Either<string, Tuple<Deck, Hand>> DrawHand(this Deck deck, int numberOfCards)
        {
            return deck.DrawHand(new Hand(), numberOfCards);
        }

        public static Deck StartDeck()
        {
            return CardsExtension.CreateListOfAllCards().StartDeck();
        }

        public static Deck StartDeck(this IEnumerable<Card> cards)
        {
            var shuffledCards = cards.Clone().Shuffle().ToList();
            return new Deck(shuffledCards);
        }

        private static Tuple<Deck, Hand> DrawHand(this Deck deck, Hand hand, int numberOfCards, int counter)
        {
            var results = deck.DrawCardForHand(hand);
            return
                counter == numberOfCards
                    ? results
                    : results.Item1.DrawHand(results.Item2, numberOfCards, ++counter);
        }
    }
}