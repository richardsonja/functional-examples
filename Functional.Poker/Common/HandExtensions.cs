﻿using System;
using System.Collections.Generic;
using Functional.Poker.Models;
using System.Linq;

namespace Functional.Poker.Common
{
    public static class HandExtensions
    {
        public static Hand AddCard(this Hand hand, Card card) => new(hand.Cards.Append((Card)card.Clone()));
        public static Hand RemoveCard(this Hand hand, Card card) => new(hand.Cards.Clone().ToList().RemoveItem((Card)card.Clone()));
        public static Hand RemoveCard(this Hand hand, IEnumerable<Card> cards)
        {
            var newHandOfCards = hand.Cards
                .Clone()
                .Except(cards, new CardComparer())
                .ToList();

            return new(newHandOfCards);
        }
    }

    public class CardComparer : IEqualityComparer<Card>
    {
        public bool Equals(Card card1, Card card2) =>
            card1!= null 
            && card2 !=null 
            && card1.Suit == card2.Suit 
            && card1.Value == card2.Value;
        public int GetHashCode(Card obj) => HashCode.Combine(obj.Suit, obj.Value);
    }
}