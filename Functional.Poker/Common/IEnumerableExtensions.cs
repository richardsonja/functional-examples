﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace Functional.Poker.Common
{
    public static class IEnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> values) => 
            values == null || !values.Any();

        public static IEnumerable<T> Clone<T>(this IEnumerable<T> listToClone) where T : ICloneable => 
            listToClone?.Select(item => (T)item.Clone())?.ToList();
        
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> values) where T : ICloneable
        {
            if (values.IsNullOrEmpty())
            {
                return new List<T>();
            }

            using var provider = new RNGCryptoServiceProvider();
            var list = values.Clone().ToList();
            var n = list.Count;
            while (n > 1)
            {
                var box = new byte[1];
                do
                {
                    provider.GetBytes(box);
                } while (!(box[0] < n * (byte.MaxValue / n)));

                var k = (box[0] % n);
                n--;
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }
    }
}