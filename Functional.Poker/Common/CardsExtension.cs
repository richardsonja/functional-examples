﻿using Functional.Poker.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using static System.Enum;

namespace Functional.Poker.Common
{
    public static class CardsExtension
    {
        public static IEnumerable<Card> CreateListOfAllCards()
        {
            var suits = GetValues(typeof(CardSuit))
                .Cast<CardSuit>()
                .ToImmutableList();
            var values = GetValues(typeof(CardValue))
                .Cast<CardValue>()
                .ToImmutableList();

            if (suits.Count() >= values.Count())
            {
                return suits
                    .SelectMany(suit =>
                        values.Select(value => new Card(value, suit)))
                    .ToList();
            }

            return values
                .SelectMany(value =>
                    suits.Select(suit => new Card(value, suit)))
                .ToList();
        }

        public static IEnumerable<TResult> SelectConsecutive<TSource, TResult>(
                    this IEnumerable<TSource> source,
            Func<TSource, TSource, TResult> selector)
        {
            if (source == null)
            {
                throw new NullReferenceException(nameof(source));
            }
            if (selector == null)
            {
                throw new NullReferenceException(nameof(selector));
            }

            var index = -1;
            var sources = source.ToList();
            foreach (var element in sources.Take(sources.Count() - 1)) // skip the last, it will be evaluated by source.ElementAt(index + 1)
            {
                checked { index++; } // explicitly enable overflow checking
                yield return selector(element, sources.ElementAt(index + 1)); // delegate element and element[+1] to Func<TSource, TSource, TResult>
            }
        }

        public static IEnumerable<Card> RemoveItem(this IEnumerable<Card> values, Card itemToRemove) 
        {
            var newValues = values.Clone().Where(x=> x.Suit != itemToRemove.Suit && x.Value != itemToRemove.Value).ToList();
            return newValues;
        }
        public static IEnumerable<KeyValuePair<CardValue, int>> ToKindAndQuantities(this IEnumerable<Card> cards)
        {
            var dict = new ConcurrentDictionary<CardValue, int>();
            if (cards == null)
            {
                return dict;
            }

            foreach (var card in cards)
            {
                // Add the value to the dictionary, or increase the count
                dict.AddOrUpdate(card.Value, 1, (cardValue, quantity) => ++quantity);
            }

            return dict;
        }
    }
}