﻿using System;

namespace Functional.Poker.Models
{
    public class Card : ICloneable
    {
        public Card(CardValue value, CardSuit suit)
        {
            Value = value;
            Suit = suit;
        }

        public CardSuit Suit { get; }

        public CardValue Value { get; }

        public override string ToString() => $"{Value} of {Suit}";
        public object Clone()
        {
            return new Card(Value, Suit);
        }
    }
}