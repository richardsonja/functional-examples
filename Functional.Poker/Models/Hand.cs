﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace Functional.Poker.Models
{
    public class Hand
    {
        public Hand() : this(new List<Card>())
        {
        }

        public Hand(IEnumerable<Card> handOfCards) => Cards = handOfCards?.ToImmutableList() ?? new List<Card>().ToImmutableList();

        public ImmutableList<Card> Cards { get; }
    }
}