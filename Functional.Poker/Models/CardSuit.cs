﻿namespace Functional.Poker.Models
{
    public enum CardSuit
    {
        Spades,
        Diamonds,
        Clubs,
        Hearts
    }
}