﻿using Functional.Poker.Common;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Functional.Poker.Models
{
    public class Deck
    {
        public Deck(IEnumerable<Card> deckOfCards)
        {
            AvailableCards = deckOfCards.Clone().ToImmutableList();
        }

        public ImmutableList<Card> AvailableCards { get; }
    }
}