using FluentAssertions;
using Functional.Poker.Common;
using Functional.Poker.Tests.Fixtures;
using System;
using System.Linq;
using Xunit;

namespace Functional.Poker.Tests.Common
{
    public class DeckExtensionsTests :
        IClassFixture<CardFixture>,
        IClassFixture<HandFixture>
    {
        private readonly CardFixture _cards;
        private readonly HandFixture _hand;

        public DeckExtensionsTests(CardFixture cards, HandFixture hand)
        {
            _cards = cards;
            _hand = hand;
        }

        [Fact]
        public void DeckExtension_DrawCardForHand_ShouldRemoveOneCardFromDeckAndAddToHand_GivenValidSetup()
        {
            _cards.Reset();
            _cards.CreateHighCardList();

            var deck = _cards.Cards.StartDeck();

            var results = deck.DrawCardForHand(_hand.Hand);
            results.Should().NotBeNull();

            var newDeck = results.Item1;
            var newHand = results.Item2;

            newDeck.Should().NotBeNull();
            newHand.Should().NotBeNull();

            deck.AvailableCards.Should().HaveCount(5);
            newDeck.AvailableCards.Should().HaveCount(4);

            _hand.Hand.Cards.Should().BeEmpty();
            newHand.Cards.Should().HaveCount(1);
        }

        [Fact]
        public void DeckExtension_DrawFullHand_ShouldPopulateTwoHands_GivenOneDeck()
        {
            var numberOfCardsToDraw = 5;
            var numberOfCards = CardsExtension.CreateListOfAllCards().Count();
            var player1Results = DeckExtensions
                .StartDeck()
                .DrawHand(numberOfCardsToDraw);
            player1Results.IsLeft.Should().BeFalse();

            var (player1Deck, playerHand1) = player1Results.Match(x => x, _ => null);

            var deckCountAfterPlayer1 = player1Deck.AvailableCards.Count;
            var player1HandCount = playerHand1.Cards.Count;
            (deckCountAfterPlayer1 + player1HandCount).Should().Be(numberOfCards);
            player1HandCount.Should().Be(numberOfCardsToDraw);

            var player2Results = player1Deck
                .DrawHand(numberOfCardsToDraw);
            player2Results.IsLeft.Should().BeFalse();
            var (player2Deck, playerHand2) = player2Results.Match(x => x, _ => null);
            player1Deck.Should().NotBe(player2Deck);

            var deckCountAfterPlayer2 = player2Deck.AvailableCards.Count;
            var player2HandCount = playerHand2.Cards.Count;
            (deckCountAfterPlayer2 + player1HandCount + player2HandCount).Should().Be(numberOfCards);
            player2HandCount.Should().Be(numberOfCardsToDraw);
        }

        [Fact]
        public void DeckExtension_DrawFullHand_ShouldReturnErrorMessage_GivenRequestedHandBeingZero()
        {
            var numberOfCardsToDraw = 0;
            var player1Results = DeckExtensions
                .StartDeck()
                .DrawHand(numberOfCardsToDraw);
            player1Results.IsLeft.Should().BeTrue();
            player1Results.IsRight.Should().BeFalse();
        }

        [Fact]
        public void DeckExtension_DrawFullHand_ShouldReturnErrorMessage_GivenRequestedHandLargerThanDeckAvailableCards()
        {
            var numberOfCards = CardsExtension.CreateListOfAllCards().Count();
            var numberOfCardsToDraw = numberOfCards + 1;
            var player1Results = DeckExtensions
                .StartDeck()
                .DrawHand(numberOfCardsToDraw);
            player1Results.IsLeft.Should().BeTrue();
            player1Results.IsRight.Should().BeFalse();
        }

        [Fact]
        public void DeckExtension_DrawFullHand_ShouldReturnNewPopulatedHand_GivenValidSetup()
        {
            var numberOfCardsToDraw = 5;
            var results = DeckExtensions
                .StartDeck()
                .DrawHand(numberOfCardsToDraw);
            results.IsLeft.Should().BeFalse();

            var numberOfCards = CardsExtension.CreateListOfAllCards().Count();

            var (deck, hand) = results.Match(x => x, _ => null);

            (deck.AvailableCards.Count + hand.Cards.Count).Should().Be(numberOfCards);
            hand.Cards.Count.Should().Be(numberOfCardsToDraw);
        }

        [Fact]
        public void DeckExtension_StartDeck_ShouldFullDeck_GivenNoSeedingCards()
        {
            var results = DeckExtensions.StartDeck();
            var numberOfCards = CardsExtension.CreateListOfAllCards().Count();

            results.Should().NotBeNull();

            results.AvailableCards.Should().HaveCount(numberOfCards);
        }

        [Fact]
        public void DeckExtension_StartDeck_ShouldReturnEmptyDeck_GivenEmptyList()
        {
            _cards.Reset();
            _cards.CreateEmptyList();

            var results = _cards.Cards.StartDeck();
            results.Should().NotBeNull();

            results.AvailableCards.Should().BeEmpty();
        }

        [Fact]
        public void DeckExtension_StartDeck_ShouldReturnEmptyDeck_GivenNullList()
        {
            _cards.Reset();
            _cards.CreateNullList();

            var results = _cards.Cards.StartDeck();
            results.Should().NotBeNull();

            results.AvailableCards.Should().BeEmpty();
        }

        [Fact]
        public void DeckExtension_StartDeck_ShouldShuffleCards_GivenListOfCards()
        {
            _cards.Reset();
            _cards.CreateHighCardList();

            var results = _cards.Cards.StartDeck();
            results.Should().NotBeNull();

            results.AvailableCards.SequenceEqual(_cards.Cards).Should().BeFalse();
        }
    }
}