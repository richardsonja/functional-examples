using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Functional.Poker.Common;
using Functional.Poker.Models;
using Functional.Poker.Tests.Fixtures;
using Xunit;

namespace Functional.Poker.Tests.Common
{
    public class IEnumerableExtensionsTests: IClassFixture<CardFixture>
    {
        private readonly CardFixture _card;
        private readonly Func<Card, Card, bool> _selector = (n, next) => n.Value + 1 == next.Value;
        public IEnumerableExtensionsTests(CardFixture card)
        {
            _card = card;
        }

        [Fact]
        public void IEnumerableExtension_Clone_ShouldNotBeSameList_GivenListOfCards()
        {
            _card.Reset();
            _card.CreateHighCardList();

            var results = _card.Cards.Clone().ToList();
            results.Should().NotBeNullOrEmpty();
            results.SequenceEqual(_card.Cards).Should().BeFalse();
        }

        [Fact]
        public void IEnumerableExtension_Shuffle_ShouldNotBeSameList_GivenListOfCards()
        {
            _card.Reset();
            _card.CreateHighCardList();

            var results = _card.Cards.Shuffle().ToList();
            results.Should().NotBeNullOrEmpty();
            results.SequenceEqual(_card.Cards).Should().BeFalse();
        }

        [Fact]
        public void IEnumerableExtension_IsNullOrEmpty_ShouldFalse_GivenListOfCards()
        {
            _card.Reset();
            _card.CreateHighCardList();

            var results = _card.Cards.IsNullOrEmpty();
            results.Should().BeFalse();
        }

        [Fact]
        public void IEnumerableExtension_IsNullOrEmpty_ShouldTrue_GivenEmptyListOfCards()
        {
            _card.Reset();
            _card.CreateEmptyList();

            var results = _card.Cards.IsNullOrEmpty();
            results.Should().BeTrue();
        }


        [Fact]
        public void IEnumerableExtension_IsNullOrEmpty_ShouldTrue_GivenNullList()
        {
            _card.Reset();
            _card.CreateNullList();

            var results = _card.Cards.IsNullOrEmpty();
            results.Should().BeTrue();
        }
    }
}