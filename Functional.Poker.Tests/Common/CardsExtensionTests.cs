using FluentAssertions;
using Functional.Poker.Common;
using Functional.Poker.Models;
using Functional.Poker.Tests.Fixtures;
using System;
using System.Linq;
using Xunit;

namespace Functional.Poker.Tests.Common
{
    public class CardsExtensionTests : IClassFixture<CardFixture>
    {
        private readonly CardFixture _card;

        private readonly Func<Card, Card, bool> _selector = (n, next) => n.Value + 1 == next.Value;

        public CardsExtensionTests(CardFixture card)
        {
            _card = card;
        }

        [Fact]
        public void CardsExtension_SelectConsecutive_ShouldAllBeConsecutive_GivenCards()
        {
            _card.Reset();
            _card.CreateConsecutiveList();

            var results = _card.Cards.OrderBy(x => x.Value).SelectConsecutive(_selector).ToList();
            results.Any(x => x).Should().BeTrue();
            results.All(x => x).Should().BeTrue();
        }

        [Fact]
        public void CardsExtension_SelectConsecutive_ShouldHaveNothing_GivenEMptyList()
        {
            _card.Reset();
            _card.CreateEmptyList();
            var results = _card.Cards.OrderBy(x => x.Value).SelectConsecutive(_selector).ToList();

            results.Should().BeEmpty();
        }

        [Fact]
        public void CardsExtension_SelectConsecutive_ShouldHaveSomeConsecutive_GivenCards()
        {
            _card.Reset();
            _card.CreateHighCardList();

            var results = _card.Cards.OrderBy(x => x.Value).SelectConsecutive(_selector).ToList();
            results.Any(x => x).Should().BeTrue();
            results.All(x => x).Should().BeFalse();
        }

        [Fact]
        public void CardsExtension_SelectConsecutive_ShouldNoConsecutive_GivenUnorderedCards()
        {
            _card.Reset();
            _card.CreateHighCardList();

            var results = _card.Cards.SelectConsecutive(_selector).ToList();
            results.Any(x => x).Should().BeFalse();
            results.All(x => x).Should().BeFalse();
        }

        [Fact]
        public void CardsExtension_ToKindAndQuantities_ShouldHaveNothing_GivenEmptyList()
        {
            _card.Reset();
            _card.CreateEmptyList();
            var results = _card.Cards.ToKindAndQuantities().ToList();

            results.Should().BeEmpty();
        }

        [Fact]
        public void CardsExtension_ToKindAndQuantities_ShouldHaveNothing_GivenNull()
        {
            _card.Reset();
            _card.CreateNullList();
            var results = _card.Cards.ToKindAndQuantities();

            results.Should().BeEmpty();
        }

        [Fact]
        public void CardsExtension_ToKindAndQuantities_ShouldHaveValueSets_GivenCards()
        {
            _card.Reset();
            _card.CreateValueSets();

            var results = _card.Cards.ToKindAndQuantities().ToList();

            results.Should().HaveCount(5);
            results.First(x => x.Key == CardValue.Six).Value.Should().Be(2);
        }
    }
}