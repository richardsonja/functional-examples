﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Functional.Poker.Tests.Fixtures
{
   public interface IReset
   {
       void Reset();
   }
}
