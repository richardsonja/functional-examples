﻿using System;
using System.Collections.Generic;
using Functional.Poker.Models;

namespace Functional.Poker.Tests.Fixtures
{
    public class CardFixture : IDisposable, IReset
    {
        public List<Card> Cards;

        public CardFixture()
        {
            Reset();
        }

        public void Dispose()
        {
        }

        public void Reset()
        {
            CreateHighCardList();
        }

        public void CreateEmptyList()
        {
            Cards = new List<Card>();
        }

        public void CreateNullList()
        {
            Cards = null;
        }

        public void CreateHighCardList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Seven, CardSuit.Spades),
                new Card(CardValue.Ten, CardSuit.Clubs),
                new Card(CardValue.Five, CardSuit.Hearts),
                new Card(CardValue.King, CardSuit.Hearts),
                new Card(CardValue.Six, CardSuit.Hearts)
            };
        }

        public void CreateValueSets()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Seven, CardSuit.Spades),
                new Card(CardValue.Ten, CardSuit.Clubs),
                new Card(CardValue.Five, CardSuit.Hearts),
                new Card(CardValue.King, CardSuit.Hearts),
                new Card(CardValue.Six, CardSuit.Hearts),
                new Card(CardValue.Six, CardSuit.Clubs)
            };
        }

        public void CreateConsecutiveList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Five, CardSuit.Hearts),
                new Card(CardValue.Seven, CardSuit.Hearts),
                new Card(CardValue.Six, CardSuit.Hearts)
            };
        }

        public void CreateFlushList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Two, CardSuit.Spades),
                new Card(CardValue.Three, CardSuit.Spades),
                new Card(CardValue.Ace, CardSuit.Spades),
                new Card(CardValue.Five, CardSuit.Spades),
                new Card(CardValue.Six, CardSuit.Spades)
            };
        }

        public void CreateFourOfAKindList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Ten, CardSuit.Clubs),
                new Card(CardValue.Jack, CardSuit.Spades),
                new Card(CardValue.Ten, CardSuit.Spades),
                new Card(CardValue.Ten, CardSuit.Hearts),
                new Card(CardValue.Ten, CardSuit.Spades)
            };
        }

        public void CreateFullHouseList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Ten, CardSuit.Clubs),
                new Card(CardValue.Jack, CardSuit.Spades),
                new Card(CardValue.Ten, CardSuit.Spades),
                new Card(CardValue.Jack, CardSuit.Hearts),
                new Card(CardValue.Ten, CardSuit.Spades)
            };
        }

        public void CreatePairList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Ten, CardSuit.Clubs),
                new Card(CardValue.Jack, CardSuit.Spades),
                new Card(CardValue.Nine, CardSuit.Spades),
                new Card(CardValue.Ten, CardSuit.Hearts),
                new Card(CardValue.Ace, CardSuit.Spades)
            };
        }

        public void CreateRoyalFlushList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Ten, CardSuit.Spades),
                new Card(CardValue.Jack, CardSuit.Spades),
                new Card(CardValue.Queen, CardSuit.Spades),
                new Card(CardValue.King, CardSuit.Spades),
                new Card(CardValue.Ace, CardSuit.Spades)
            };
        }

        public void CreateStraightList(bool ordered)
        {
            if (ordered)
            {
                Cards = new List<Card>
                {
                    new Card(CardValue.Ten, CardSuit.Clubs),
                    new Card(CardValue.Jack, CardSuit.Spades),
                    new Card(CardValue.Queen, CardSuit.Spades),
                    new Card(CardValue.King, CardSuit.Hearts),
                    new Card(CardValue.Ace, CardSuit.Spades)
                };
            }
            else
            {
                Cards = new List<Card>
                {
                    new Card(CardValue.Queen, CardSuit.Spades),
                    new Card(CardValue.Ten, CardSuit.Clubs),
                    new Card(CardValue.King, CardSuit.Hearts),
                    new Card(CardValue.Jack, CardSuit.Spades),
                    new Card(CardValue.Ace, CardSuit.Spades)
                };
            }
        }

        public void CreateThreeOfAKindList()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Ten, CardSuit.Clubs),
                new Card(CardValue.Jack, CardSuit.Spades),
                new Card(CardValue.Nine, CardSuit.Spades),
                new Card(CardValue.Ten, CardSuit.Hearts),
                new Card(CardValue.Ten, CardSuit.Spades)
            };
        }

        public void CreateTwoPair()
        {
            Cards = new List<Card>
            {
                new Card(CardValue.Ten, CardSuit.Clubs),
                new Card(CardValue.Jack, CardSuit.Spades),
                new Card(CardValue.Jack, CardSuit.Diamonds),
                new Card(CardValue.Ten, CardSuit.Hearts),
                new Card(CardValue.Ace, CardSuit.Spades)
            };
        }
    }
}