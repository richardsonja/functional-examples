﻿using Functional.Poker.Models;
using System;
using System.Collections.Generic;

namespace Functional.Poker.Tests.Fixtures
{
    public class HandFixture : IDisposable, IReset
    {
        public Hand Hand;

        public HandFixture()
        {
            Reset();
        }

        public void CreateEmptyHand()
        {
            Hand = new Hand();
        }

        public void CreateNewHand(IEnumerable<Card> cards)
        {
            Hand = new Hand(cards);
        }

        public void Dispose()
        {
        }

        public void Reset()
        {
            CreateEmptyHand();
        }
    }
}