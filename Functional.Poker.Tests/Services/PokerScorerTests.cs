using FluentAssertions;
using Functional.Poker.Common;
using Functional.Poker.Models;
using Functional.Poker.Services;
using Functional.Poker.Tests.Fixtures;
using Xunit;

namespace Functional.Poker.Tests.Services
{
    public class PokerScorerTests : IClassFixture<CardFixture>
    {
        private readonly CardFixture _card;

        public PokerScorerTests(CardFixture card)
        {
            _card = card;
        }

        [Fact]
        public void CanGetHighCard()
        {
            _card.Reset();
            _card.CreateHighCardList();

            _card.Cards.HighCard().Value.Should().Be(CardValue.King);
        }

        [Fact]
        public void CanScoreFlush()
        {
            _card.Reset();
            _card.CreateFlushList();

            _card.Cards.GetHandRank().Should().Be(HandRank.Flush);
        }

        [Fact]
        public void CanScoreFourOfAKind()
        {
            _card.Reset();
            _card.CreateFourOfAKindList();

            _card.Cards.GetHandRank().Should().Be(HandRank.FourOfAKind);
        }

        [Fact]
        public void CanScoreFullHouse()
        {
            _card.Reset();
            _card.CreateFullHouseList();

            _card.Cards.GetHandRank().Should().Be(HandRank.FullHouse);
        }

        [Fact]
        public void CanScoreHighCard()
        {
            _card.Reset();
            _card.CreateHighCardList();

            _card.Cards.GetHandRank().Should().Be(HandRank.HighCard);
        }

        [Fact]
        public void CanScorePair()
        {
            _card.Reset();
            _card.CreatePairList();

            _card.Cards.GetHandRank().Should().Be(HandRank.Pair);
        }

        [Fact]
        public void CanScoreRoyalFlush()
        {
            _card.Reset();
            _card.CreateRoyalFlushList();
            
            _card.Cards.GetHandRank().Should().Be(HandRank.RoyalFlush);
        }

        [Fact]
        public void CanScoreStraight()
        {
            _card.Reset();
            _card.CreateStraightList(true);

            _card.Cards.GetHandRank().Should().Be(HandRank.Straight);
        }

        [Fact]
        public void CanScoreStraightUnordered()
        {
            _card.Reset();
            _card.CreateStraightList(false);

            _card.Cards.GetHandRank().Should().Be(HandRank.Straight);
        }

        [Fact]
        public void CanScoreThreeOfAKind()
        {
            _card.Reset();
            _card.CreateThreeOfAKindList();

            _card.Cards.GetHandRank().Should().Be(HandRank.ThreeOfAKind);
        }

        [Fact]
        public void CanScoreTwoPair()
        {
            _card.Reset();
            _card.CreateTwoPair();
            
            _card.Cards.GetHandRank().Should().Be(HandRank.TwoPair);
        }

        [Fact]
        public void Hand_Cards_ShouldFindHighCard_GivenCards()
        {
            _card.Reset();
            _card.CreateHighCardList();

            _card.Cards.HighCard().Value.Should().Be(CardValue.King);
        }
    }
}