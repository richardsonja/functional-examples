using FluentAssertions;
using Functional.Poker.Models;
using Xunit;

namespace Functional.Poker.Tests.Models
{
    public class CardTests
    {
        [Fact]
        public void Card_ShouldBeValid_GivenValidSetup()
        {
            var card = new Card(CardValue.Ace, CardSuit.Clubs);
            card.Suit.Should().NotBeNull();
            card.Value.Should().NotBeNull();

            card.Suit.Should().Be(CardSuit.Clubs);
            card.Value.Should().Be(CardValue.Ace);
        }

        [Fact]
        public void Card_ShouldDescribeCard_GivenValidSetup()
        {
            var card = new Card(CardValue.Ace, CardSuit.Spades);

            card.ToString().Should().Be("Ace of Spades");
        }
    }
}