using FluentAssertions;
using Functional.Poker.Models;
using System.Linq;
using Functional.Poker.Common;
using Functional.Poker.Tests.Fixtures;
using Xunit;

namespace Functional.Poker.Tests.Models
{
    public class HandTests : IClassFixture<HandFixture>
    {
        private readonly HandFixture _hand;

        public HandTests(HandFixture hand)
        {
            _hand = hand;
        }
        [Fact]
        public void Hand_Cards_ShouldBeValid_GivenValidSetup()
        {
            _hand.Reset();
            _hand.CreateEmptyHand();
            var hand = _hand.Hand;
            hand.Should().NotBeNull();
            hand.Cards.Should().BeEmpty();
        }

        [Fact]
        public void Hand_AddCard_ShouldAddANdCreateNew_GivenOneCard()
        {
            _hand.Reset();
            _hand.CreateEmptyHand();
            var card = new Card(CardValue.Ace, CardSuit.Spades);
            var hand = _hand.Hand.AddCard(card);
            hand.Should().NotBe(_hand.Hand);

            var firstCard = hand.Cards.FirstOrDefault();
            firstCard.Should().NotBeNull();
            firstCard.Should().NotBe(card);
            firstCard.Value.Should().Be(card.Value);
            firstCard.Suit.Should().Be(card.Suit);
        }

        [Fact]
        public void Hand_Draw_ShouldReturnNewCopy_GivenInputs()
        {
            var card = new Card(CardValue.Ace, CardSuit.Spades);

            var secondHand = _hand.Hand.AddCard(card);
            _hand.Hand.Cards.Should().HaveCount(0);
            secondHand.Cards.Should().HaveCount(1);
        }
    }
}