using FluentAssertions;
using Functional.Poker.Models;
using Functional.Poker.Tests.Fixtures;
using Xunit;

namespace Functional.Poker.Tests.Models
{
    public class DeckTests : IClassFixture<CardFixture>
    {
        private readonly CardFixture _card;

        public DeckTests(CardFixture card)
        {
            _card = card;
        }

        [Fact]
        public void Deck_ShouldBeValid_GivenValidSetup()
        {
            _card.Reset();
            _card.CreateHighCardList();

            var deck = new Deck(_card.Cards);
            deck.Should().NotBeNull();
        }
    }
}